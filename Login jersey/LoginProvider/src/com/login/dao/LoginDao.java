package com.login.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.login.util.DButil;

public class LoginDao {
	public String isRecordExist(String username) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String password = null;
		String sql = "select password from login_tbl where username = ?";
		try {
			// getting connection object and executing query
			con = DButil.getcon();
			ps = con.prepareStatement(sql);
			ps.setString(1, username);

			rs = ps.executeQuery();
			if (rs.next()) {
				password = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// closing connection
				con.close();
			} catch (SQLException e) {

			}
		}

		return password;
	}
}
