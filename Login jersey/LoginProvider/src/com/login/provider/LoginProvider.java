package com.login.provider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.login.service.LoginService;

@Path("/login")
public class LoginProvider {
	@GET
	public String login(@QueryParam("username") String username, @QueryParam("password") String password) {
		boolean isLogin = new LoginService().authenticateUser(username, password);
		String res = isLogin ? username : null;
		return res;
	}
}
