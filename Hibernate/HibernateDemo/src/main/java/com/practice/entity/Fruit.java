package com.practice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Fruit")
public class Fruit {
private int fruitId;
private String friuitName;
private double fruitCost;
@Id
@Column(name="fruitId")
public int getFruitId() {
	return fruitId;
}

public void setFruitId(int fruitId) {
	this.fruitId = fruitId;
}
@Column(name="fruitName")
public String getFriuitName() {
	return friuitName;
}
public void setFriuitName(String friuitName) {
	this.friuitName = friuitName;
}
@Column(name="fruitCost")
public double getFruitCost() {
	return fruitCost;
}
public void setFruitCost(double fruitCost) {
	this.fruitCost = fruitCost;
}

}
