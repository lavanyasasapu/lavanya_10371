package com.practice.main;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.practice.entity.Fruit;
import com.practice.util.HibernateUtil;

public class Main {

	public static void main(String args[]) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
//		Session session = sessionFactory.openSession();
//		org.hibernate.Transaction txn = session.beginTransaction();
//		
//		Fruit fruit = new Fruit();
//		fruit.setFruitId(2);
//		fruit.setFriuitName("Apple");
//		fruit.setFruitCost(20);
//		
//		session.save(fruit);
//		txn.commit();
//		session.close();
		//----------------------//
		
		Session session1 = sessionFactory.openSession();	
		Fruit fruit1 = 	session1.get(Fruit.class, 1);
		System.out.println(fruit1.getFriuitName());
        System.out.println(fruit1.getFruitCost());		
        session1.close();        
       Session session2 = sessionFactory.openSession();
		Fruit fruit2 = 	session2.get(Fruit.class, 1);
		System.out.println(fruit2.getFriuitName());
        System.out.println(fruit2.getFruitCost());
       
        session2.close();
        
        if(fruit1 == fruit2 ) {
        	System.out.println("same");
        }
        else {
        	System.out.println("differ");
        }
        
		
		
	}
}
