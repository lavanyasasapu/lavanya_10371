package com.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Employee")
public class Employee {
    
	@Id
    @GeneratedValue
	@Column(name = "empId")
	private int empId;
	
	@Column(name = "EmpName")
	private String name;
	
	@OneToMany(mappedBy = "Employee", cascade = CascadeType.ALL)
	private Account account;
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAccount(HashSet<Account> accountEntity1) {
		// TODO Auto-generated method stub
		
	}
	
}
