package com.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "ForeignKeyAssoAccountEntity")
@Table(name = "Account")
public class Account {
	
	@Id
	@GeneratedValue
	@Column(name = "accId")
	private int accId;
	
	@Column(name ="accNo")
	private int accNo;
	
	@Column(name= "accName")
	private String accName;
	
	 @ManyToOne
	    @JoinColumn(name = "emp_id")
	private Employee emp ;

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public int getAccNo() {
		return accNo;
	}

	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}

}
