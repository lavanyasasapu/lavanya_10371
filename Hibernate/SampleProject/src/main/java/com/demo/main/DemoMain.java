package com.demo.main;

import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.entity.Account;
import com.demo.entity.Employee;
import com.demo.util.HibernateUtil;

public class DemoMain {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		org.hibernate.Transaction txn = session.beginTransaction();
	    
		
		Account account1 = new Account();
		account1.setAccNo(12345);
		account1.setAccName("lavanya");
		
		Account account2 = new Account();
		account2.setAccName("sita");
		account2.setAccId(67890);
		
	
		Employee emp1 = new Employee();
		emp1.setName("lavanya");
		
		
		Employee emp2 = new Employee();
		emp2.setName("sita");
		
		
		HashSet<Account> accountEntity1 = new HashSet<Account>();
		accountEntity1.add(account1);
		accountEntity1.add(account2);
		
		HashSet<Account > accountEntity2 = new HashSet<Account>();
		accountEntity2.add(account2);
		
		emp1.setAccount(accountEntity1);
		emp2.setAccount(accountEntity2);
		
		
		
		session.save(emp1);
		session.save(emp2);
		
		txn.commit();
    	session.close();
	     //HibernateUtil.shutdown();

	}

}
