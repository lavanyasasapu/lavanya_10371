package com.consumer.service;

import java.util.ArrayList;
import java.util.List;

import com.consumer.dao.AdminDAOImpl;
import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
/**
 * implementation class for admin services
 * @author IMVIZAG
 *
 */
public class AdminServicesIMPL implements AdminServices {
	//creating an object for admin dao to aceess dao members
	AdminDAOImpl adminDaoImp = new AdminDAOImpl();
	Complaints complaints = new Complaints();
	/**
	 * method for adding an extra service.
	 * and it will call dao method
	 */
	public boolean addService(String service) {			
		boolean result = adminDaoImp.addExtraService(service);	
		return result;
	}
	/**
	 * Method for deleting the user service
	 *  and it will call dao method
	 */
	public boolean deleteService(int key) {
		boolean isDeleted = adminDaoImp.removeService(key);
		return isDeleted;
		
	}
	 /**
     * Method for updating the service 
     * and it  will call the dao class method
     */
	public boolean updateService(int key, String update_service) {
		boolean isUpdate = adminDaoImp.modifyService(key, update_service);
		return isUpdate;
	}
	/**
	 * Method for retrieving the total number of complaints 
	 * and it  will call the dao class method and it will return the all complaints given by the user
	 */
	public  List<Complaints> totalComplaints() {		
		List<Complaints> complaint_list = adminDaoImp.allComplaints();		
		return complaint_list;
	}
	 /**
     * Method for retrieving the total customers who are registered
     * and it  will call the dao class method and it will return the all user registrations details
     */
	public ArrayList<Registration> totalCustomers() {
		ArrayList<Registration> registration_list = adminDaoImp.allCustomers();		
		return registration_list;
	}


}
