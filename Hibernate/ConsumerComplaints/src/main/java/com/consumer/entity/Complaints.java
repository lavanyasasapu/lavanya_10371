package com.consumer.entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *  pojo class for storing user complaints
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "Complaints")
public class Complaints {
	@Override
	public String toString() {
		return "Complaints [reference_Id=" + reference_Id + ", complaint_Name=" + complaint_Name + ", complaint_id="
				+ complaint_id + ", consumer_Id=" + consumer_Id + ", service_Id=" + service_Id + ", Date_of_Complaint="
				+ Date_of_Complaint + "]";
	}
	@Id
	@Column(name = "reference_id")
	private int reference_Id;
	
	@Column(name = "complaint_name")
	private String complaint_Name;
	
	@Column(name = "complaint_id")
	private int complaint_id;
	
	@Column(name = "consumer_id")
	private String consumer_Id;
	
	@Column(name = "service_id")
	private int service_Id;
	
	@Column(name = "date_of_complaint")
	private Date Date_of_Complaint;
	
	public int getReference_Id() {
		return reference_Id;
	}
	public void setReference_Id(int reference_Id) {
		this.reference_Id = reference_Id;
	}
	public String getComplaint_Name() {
		return complaint_Name;
	}
	public void setComplaint_Name(String complaint_Name) {
		this.complaint_Name = complaint_Name;
	}
	public int getComplaint_id() {
		return complaint_id;
	}
	public void setComplaint_id(int complaint_id) {
		this.complaint_id = complaint_id;
	}
	public String getConsumer_Id() {
		return consumer_Id;
	}
	public void setConsumer_Id(String consumer_Id) {
		this.consumer_Id = consumer_Id;
	}
	public int getService_Id() {
		return service_Id;
	}
	public void setService_Id(int service_Id) {
		this.service_Id = service_Id;
	}
	public Date getDate_of_Complaint() {
		return  Date_of_Complaint;
	}
	public void setDate_of_Complaint(Date date) {
		Date_of_Complaint = date;
	}
	
}
