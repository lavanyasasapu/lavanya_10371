package com.consumer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *pojo class for new registration
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "registration")
public class Registration {
	@Id
	@Column(name = "id")
	String cus_id;
	@Column(name = "password")
	String pwd;
	@Column(name = "aadhar")
	String aadhar_num;
	
	// getter and setter methods
	public String getCus_id() {
		return cus_id;
	}

	public void setCus_id(String cus_id) {
		this.cus_id = cus_id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAadhar_num() {
		return aadhar_num;
	}

	public void setAadhar_num(String aadhar_num) {
		this.aadhar_num = aadhar_num;
	}
}
