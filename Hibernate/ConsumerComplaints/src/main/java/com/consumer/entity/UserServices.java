package com.consumer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * pojo class for storing User Services
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "user_services")
public class UserServices {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "service_id")
	private int serviceID;
	
	@Column(name = "Service_Name")
	private String service_name;

	//getters and setters
	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	
	

}