package com.consumer.provider;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
import com.consumer.service.AdminServicesIMPL;
import com.google.gson.Gson;

/**
 * This is the resource class which acts as web resource which is specified with
 * a URL
 * 
 * @author IMVIZAG
 *
 */

@Path("/provider")

public class AdminProvider {


	/**
	 * This is resource method for adding the service by the Admin
	 * 
	 * @param service_name
	 * @return
	 */

	@POST
	@Path("/addService")
	public String addService(@QueryParam("service_name") String service_name) {
		System.out.println("heloo");

		boolean isAdded = new AdminServicesIMPL().addService(service_name);

		return isAdded ? "Service Added" : "Something wriong";

	}

	/**
	 * This is resource method for deleting the service by the Admin
	 * 
	 * @param service_name
	 * @return
	 */

	@DELETE
	@Path("/deleteService")
	public String deleteService(@QueryParam("key") int key) {
		System.out.println("delete provider");

		boolean isDelete = new AdminServicesIMPL().deleteService(key);

		return isDelete ? "Service Deleted" : "Something wrong ";

	}

	/**
	 * This is resource method for updating the service by the Admin
	 * 
	 * @param update_service
	 * @param key
	 * @return
	 */

	@POST
	@Path("/updateService")
	public String deleteService(@QueryParam("key") int key, @QueryParam("update_service") String update_service) {

		boolean isUpdate = new AdminServicesIMPL().updateService(key, update_service);
		return isUpdate ? "Service Updated" : "Something wrong ";

	}

	/**
	 * This is resource method for displaying the total number of complaints made by
	 * the user
	 * 
	 * @return
	 */

	@GET
//	@Produces("MediaType/json")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/totalComplaints")
	public String totalComplaints() {
        Gson gson = new Gson();
		List<Complaints> totalComplaints = new AdminServicesIMPL().totalComplaints();
		String total = gson.toJson(totalComplaints);
		return total;
	}

	/**
	 * This is resource method for displaying all the registered users
	 * 
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Path("/totalCustomers")
	public ArrayList<Registration> totalCustomers() {

		ArrayList<Registration> totalCustomers = new AdminServicesIMPL().totalCustomers();
		return totalCustomers;

	}

}
