package com.consumer.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.consumer.entity.Complaints;
import com.consumer.entity.Registration;
import com.consumer.entity.UserServices;
import com.consumer.util.HibernateUtil;

/**
 * Dao class for admin
 * 
 * @author IMVIZAG
 *
 */
public class AdminDAOImpl implements AdminDAO {
	/**
	 * method for add extra service which can be updated to the User Services Directly.
	 */
	public boolean addExtraService(String service) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		UserServices userServices = new UserServices();
		userServices.setService_name(service);
		session.save(userServices);
		System.out.println("saved");
		txn.commit();
		session.close();
		return true;	
	}

	/**
	 * method for removing the service.
	 * After Removing the Service it will automatically updated to the User Services.
	 */
	public boolean removeService(int key) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		UserServices userServices = session.get(UserServices.class, key);
		System.out.println("delete dao");
		session.delete(userServices);
		System.out.println("Deleted");
		txn.commit();
		session.close();
		return true;
		
	}

	/**
	 * method for updating the service based on the key.
	 * Here Existing service can be updated and it will updated to User Services.
	 */
	public boolean modifyService(int key, String update_service) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		UserServices userServices = new UserServices();
		session.update(key);
		System.out.println("Updated");
		txn.commit();
		session.close();
		return true;
		
	}

	/**
	 * method for retriving all the complaints details.
	 */
	public List<Complaints> allComplaints() {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Complaints> list = session.createQuery("FROM Complaints").list();
//		Iterator<Complaints> iterator = list.iterator();
//		while (iterator.hasNext()) {
//			Complaints complaints = (Complaints) iterator.next();
////			System.out.println("reference_Id"+complaints.getReference_Id()+"complaint_Name"+complaints.getComplaint_Name()+
////					"complaint_id"+complaints.getComplaint_id()+"consumer_Id"+complaints.getConsumer_Id()+"service_Id"+complaints.getService_Id()+
////					"Date_of_Complaint"+complaints.getDate_of_Complaint());
//			
//			list.add(complaints);
//		}
		System.out.println("coming into the dao");
		txn.commit();
		session.close();
		return list;
		
	}

	/**
	 * method for retrieving all the Registered customers Details.
	 */
	public ArrayList<Registration> allCustomers() {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		List<Complaints> allCustomers = session.createQuery("FROM Product").list();
		Iterator<Complaints> iterator = allCustomers.iterator();
		while (iterator.hasNext()) {
			Registration registration = new Registration();
			System.out.println("Consumer ID"+registration.getCus_id()+"Password"+registration.getPwd()+"Aadhar"+registration.getAadhar_num());
		}
		txn.commit();
		session.close();
		return null;
	
	}


	
	
}
