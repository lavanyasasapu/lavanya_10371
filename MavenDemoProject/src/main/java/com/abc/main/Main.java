package com.abc.main;

import java.util.Scanner;

import com.abc.entity.Product;
import com.abc.service.ProductService;

public class Main {
	
	public static void main(String[] args) {
		
		//for create the product
		Product product = new Product();
		product.setProduct_id("P105");
		product.setProduct_name("REDMI");
		product.setProduct_price(30000);
		product.setProduct_category("Mobiles");
		ProductService productService = new ProductService();
		productService.createProduct(product);
		System.out.println("Done....");
		
		//for display all the products
		productService.listAllProducts();
	
		//for update the product
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the product id");
		String product_id = sc.next();
		System.out.println("enter the product name");
		String product_name = sc.next();
	   productService.updateProduct(product_id, product_name);	
		
	}

}
 