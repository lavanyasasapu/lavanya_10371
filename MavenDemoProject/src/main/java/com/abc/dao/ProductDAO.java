package com.abc.dao;



import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

public class ProductDAO {
	
	public boolean create(Product product) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		//creating a session
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(product);
		System.out.println("product saved");
		txn.commit();
		session.close();
		return true;
	}
	
	public void displayProduct() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction trans = session.beginTransaction();
		//List<Product> product_list =  session.createQuery("select product_name from product").list();
		Query query = session.createQuery("FROM Product");
		List product_list =  query.list();
		Iterator<Product> itr = product_list.iterator();
		while(itr.hasNext()) {
			 Product product = new Product();
			 product =  itr.next();
			 System.out.println("Name    :"+product.getProduct_name());
			 System.out.println("Id      :" +product.getProduct_id());
			 System.out.println("price   :"+product.getProduct_price());
			 System.out.println("category:"+product.getProduct_category());
			 System.out.println();
			
		}
		trans.commit();
		session.close();
	}
	
	public void updateProduct(String product_id , String product_name) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction trans = session.beginTransaction();
		Product product = session.get(Product.class, product_id);
		product.setProduct_name(product_name);
		session.update(product);
		System.out.println("Product name updated...");
		trans.commit();
		session.close();
	}

}
