package com.abc.service;

import com.abc.dao.ProductDAO;
import com.abc.entity.Product;
/**
 * This is a service method for product class
 * @author IMVIZAG
 *
 */
public class ProductService {
	
	ProductDAO productDAO;
	
	public ProductService() {
		
		productDAO = new ProductDAO();
	}
	/**
	 * Method to create a new product
	 * @param product
	 * @return
	 */
	public boolean createProduct(Product product) {
		return productDAO.create(product);
		
	}
	/**
	 * Method to display all products
	 */
   public void  listAllProducts() {
	   productDAO.displayProduct();
	   
   }
   /**
    * method to update the product
    * @param product_id
    * @param product_name
    */
   public void updateProduct(String product_id, String product_name) {
	   productDAO.updateProduct(product_id, product_name);
   }
}
