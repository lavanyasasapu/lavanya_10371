package com.mobilebill.dao;

import java.util.List;
import com.mobilebill.bean.Admin;

/**
 * this is the interface in which all the abstract methods are declared
 * 
 * @author RANJHS
 *
 */

public interface AdminRegistration {

	boolean createAdmin(Admin admin);

	List<Admin> displayAdmin();

}