package com.mobilebill.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilebill.bean.Adminplans;
import com.mobilebill.util.DBUtil;

/**
 * This class is used for adding the plans by the admin
 * 
 * @author RANJHS
 *
 */

public class AdminPlansOperations {
	Connection con = null;

	public void doAddingPlans(Adminplans ap, int packType) {
		con = DBUtil.getCon();
		PreparedStatement ps = null;

		String sql = "insert into plans (plan_name,data,calls,amazon_prime,netflix,amount,validity,plan_category_id) values (?,?,?,?,?,?,?,?)";
		try {
			ps = con.prepareStatement(sql);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			@SuppressWarnings("unused")
			int result;
			ps.setString(1, ap.getPlan_name());
			ps.setString(2, ap.getData());
			ps.setInt(3, ap.getCalls());
			ps.setString(4, ap.getAmazon_prime());
			ps.setString(5, ap.getNetfix());
			ps.setDouble(6, ap.getAmount());
			ps.setInt(7, ap.getValidity());
			ps.setInt(8, packType);
//			ps.setInt(9,3);

			result = ps.executeUpdate();
			System.out.println("succesfully inserted data ");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * This method displays all the plans
	 * 
	 * @param packType
	 * @return It returns the list
	 */

	public ArrayList<Adminplans> doDisplayPlans(int packType) {

		con = DBUtil.getCon();
		ArrayList<Adminplans> al = new ArrayList<Adminplans>();
		Adminplans as = null;

		Statement stmt1 = null;
		try {
			stmt1 = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			ResultSet rs = stmt1.executeQuery("\r\n"
					+ "select pc.pack_name ,pl.plan_name,pl.data,pl.calls,pl.amazon_prime,pl.netflix,pl.amount,pl.validity from plans as pl inner join plan_category as pc on pc.plan_category_id="
					+ packType + " and pl.plan_category_id=" + packType);

			while (rs.next()) {
				as = new Adminplans();
				as.setPackType(rs.getString(1));
				as.setPlan_name(rs.getString(2));
				as.setData(rs.getString(3));
				as.setCalls(rs.getInt(4));
				as.setAmazon_prime(rs.getString(5));
				as.setNetfix(rs.getString(6));
				as.setAmount(rs.getInt(7));
				as.setValidity(rs.getInt(8));

				al.add(as);
			}
//				System.out.printf("%-20d %-10s %-20s \n", rs.getInt(2), rs.getString(3),rs.getString(4)); 
//				al.add()

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		// returns the result to the controller package
		return al;

	}

	/**
	 * This method deletes the plans
	 * 
	 * @param planType
	 * @param planName
	 */

	public void doDeletePlans(int planType, String planName) {
		PreparedStatement ps = null;
		con = DBUtil.getCon();
		try {
			ps = con.prepareStatement("DELETE FROM plans WHERE plan_name= ? and plan_category_id=?");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			ps.setString(1, planName);
			ps.setInt(2, planType);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int result_set = 0;
		try {
			result_set = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result_set > 0) {
			System.out.println("Deleted");
		} else {
			System.out.println("Can't delete");
		}
	}
}
