package com.mobilebill.bean;

/**
 * This AdminPlan class is used to declare,set the values for the variables and
 * retrive them
 * 
 * @author RANJHS
 *
 */

public class Adminplans {
	// declaring the variables

	private String plan_name;
	private String data;
	private int calls;
	private String amazon_prime;
	private String netfix;
	private double amount;
	private int validity;
	private String packType;

	// getter and setter methods

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getCalls() {
		return calls;
	}

	public void setCalls(int calls) {
		this.calls = calls;
	}

	public String getAmazon_prime() {
		return amazon_prime;
	}

	public void setAmazon_prime(String amazon_prime) {
		this.amazon_prime = amazon_prime;
	}

	public String getNetfix() {
		return netfix;
	}

	public void setNetfix(String netfix) {
		this.netfix = netfix;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}

	public String getPackType() {
		return packType;
	}

	public void setPackType(String packType) {
		this.packType = packType;
	}

}
