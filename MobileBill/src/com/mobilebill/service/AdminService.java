package com.mobilebill.service;

import java.util.List;

import com.mobilebill.bean.Admin;

/**
 * this is the interface in which all the abstract methods are declared
 * 
 * @author RANJHS
 *
 */

public interface AdminService {

	boolean insertAdmin(Admin admin);

	List<Admin> FetchAdmin();

	boolean doLogin(String username);

	boolean doLogin(String username, String password);

	boolean updatePassword(String username, String password);

}
