package com.mobilebill.service;

import java.sql.Date;

import com.mobilebill.dao.UserDAO;

/**
 * this class is used to get and set the details
 * 
 * @author IMVIZAG
 *
 */
public class UserService {
	/**
	 * gets users service id
	 * 
	 * @param mobileNumber
	 * @return
	 */
	public static int validateUser(String mobileNumber) {

		int service_id = UserDAO.validateUser(mobileNumber);
		return service_id;
	}

	/**
	 * gets network of user
	 * 
	 * @param mobileNumber
	 * @return
	 */
	public static String networkOfUser(String mobileNumber) {
		String status = UserDAO.getStatus(mobileNumber);
		String network = null;
		if (status.equals("active")) {
			network = UserDAO.networkOfUser(mobileNumber);
		} else {
			network = "blocked";
		}
		return network;
	}

	/**
	 * checks whether users is registered or not
	 * 
	 * @param mobileNumber
	 * @return
	 */
	public static boolean registeredUser(String mobileNumber) {

		boolean result = UserDAO.registeredUser(mobileNumber);
		return result;
	}

	/**
	 * sets the new user
	 * 
	 * @param mobileNumber
	 * @param name
	 * @param gender
	 * @param dob
	 * @param service_id
	 * @return
	 */
	public static boolean userRegistration(String mobileNumber, String name, String gender, Date dob, int service_id) {
		boolean result = UserDAO.userRegistration(mobileNumber, name, gender, dob, service_id);
		return result;
	}
	
	public static boolean blockUser(String mobilenumber) {
		
		int user_id = UserDAO.getUserId(mobilenumber);
		
		return UserDAO.blockUser(user_id);
		
	}

}
