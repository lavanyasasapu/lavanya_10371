package com.login.consumer;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * This class contains consumer code which is responsible for using web services
 * 
 * @author IMVIZAG
 *
 */
public class LoginConsumer {
	/**
	 * This method makes a Login request to the web service identified by respective
	 * url
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public String doLoginRequest(String username, String password) {
		// preparing url
		String url = "http://localhost:2019/LoginProvider/rest/login?username=venki03&password=venki@03";
		// creating empty client object
		Client client = Client.create();
		// Adding url to the client object which returns web resource object
		WebResource resource = client.resource(url);
		
		ClientResponse res = resource.post(ClientResponse.class);
		String str = res.getEntity(String.class);
		return str;
	} // End of doLoginRequest
	
}
