package com.login.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.login.consumer.LoginConsumer;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//retrieving data form request
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		//calling consumer method
		String uname = new LoginConsumer().doLoginRequest(username, password);
		RequestDispatcher rd = request.getRequestDispatcher("Welcome.jsp");
		request.setAttribute("username", uname);
		if(uname != null) {
			rd.forward(request, response);
		}else {
			response.sendRedirect("error.html");
		}
	}

}
