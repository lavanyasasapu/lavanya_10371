package com.consumercomplaints.services;

import java.util.ArrayList;
import java.util.Map;

import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
/**
*  interface for UserServices
*  it having abstratct methods as like below
*  and its implentation is provided in UserServices implementation class
* @author IMVIZAG
*
*/
public interface UserServices {
	
	boolean  newUserRegistration(Registration register);
  	boolean login(String id, String password);
  	String checkPerson();
  	boolean findById(String id);
  	boolean validateAadhar(String aadhar);
	Map<Integer, String> fetchAllServices();
	boolean setComplaint(int service_Id, String complaint_Name,int complaint_id,String user_id);
	ArrayList<UpdatedComplaints> myUpdatedComplaintStatus(String user_id);
	boolean updatingMyPassword(String consumer_id, String password);
}
