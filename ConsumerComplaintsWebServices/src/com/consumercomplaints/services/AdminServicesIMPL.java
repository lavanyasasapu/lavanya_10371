package com.consumercomplaints.services;

import java.util.ArrayList;
import java.util.Date;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.dao.AdminDAOImpl;
/**
 * implementation class for admin services
 * @author IMVIZAG
 *
 */
public class AdminServicesIMPL implements AdminServices {
	//creating an object for admin dao to aceess dao members
	AdminDAOImpl adminDaoImp = new AdminDAOImpl();
	Complaints complaints = new Complaints();
	/**
	 * method for adding an extra service.
	 * and it will call dao method
	 */
	public boolean addService(String service) {			
		boolean result = adminDaoImp.addExtraService(service);	
		return result;
	}
	/**
	 * Method for deleting the user service
	 *  and it will call dao method
	 */
	public boolean deleteService(int key) {
		boolean isDeleted = adminDaoImp.removeService(key);
		return isDeleted;
		
	}
	 /**
     * Method for updating the service 
     * and it  will call the dao class method
     */
	public boolean updateService(int key, String update_service) {
		boolean isUpdate = adminDaoImp.modifySerive(key, update_service);
		return isUpdate;
	}
	/**
	 * Method for retrieving the total number of complaints 
	 * and it  will call the dao class method and it will return the all complaints given by the user
	 */
	public  ArrayList<Complaints> totalComplaints() {		
		ArrayList<Complaints> complaint_list = adminDaoImp.allComplaints();		
		return complaint_list;
	}
	 /**
     * Method for retrieving the total customers who are registered
     * and it  will call the dao class method and it will return the all user registrations details
     */
	public ArrayList<Registration> totalCustomers() {
		ArrayList<Registration> registration_list = adminDaoImp.allCustomers();		
		return registration_list;
	}

	public Complaints checkByReferenceId(int reference_id) {
		complaints = adminDaoImp.modifyCompliantStatus(reference_id);
		return complaints;
	}
	
	public ArrayList<Complaints> checkByServiceId(String service_id) {
		ArrayList<Complaints> complaint_list = adminDaoImp.allComplaintsByServiceId(service_id);
		return complaint_list;
	}

	public ArrayList<Complaints> checkByConsumerId(String consumer_id) {
		ArrayList<Complaints> complaint_list = adminDaoImp.allComplaintsByConusumerId(consumer_id);
		return complaint_list;
	}

	/**
     * method for setiing the complaint status
     */
	public boolean setComplaintStatus(int reference_id, String consumer_id, String status, Date date, String comment) {
		
		UpdatedComplaints updatedComplaints = new UpdatedComplaints();
		updatedComplaints.setReference_id(reference_id);
		updatedComplaints.setStatus(status);
		updatedComplaints.setDate_of_resolving(date);
		updatedComplaints.setComments(comment);
		
		return adminDaoImp.insertUpdatedComplaint(updatedComplaints);
	}
}
