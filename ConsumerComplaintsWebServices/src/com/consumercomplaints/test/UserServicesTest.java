package com.consumercomplaints.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.services.AdminServicesIMPL;
import com.consumercomplaints.services.UserServices;
import com.consumercomplaints.services.UserServicesIMPL;

public class UserServicesTest {
	UserServices userServices = null;

	/**
	 * setUp assert method to execute before each method is invoked
	 */
	@Before
	public void setUp() {
		userServices = new UserServicesIMPL();
	}
	
	/**
	 *  assert method to execute after each method is invoked
	 */
	@After
	public void tearDown() {
		userServices = null;
	}
	
	/**
	 * test case for new user registration
	 */
	@Ignore
	@Test
	public void  newRegistration() {
	    Registration register = new Registration();
	    register.setCus_id("121");
	    register.setPwd("rithika120");
	    register.setAadhar_num("798920030212");
		Assert.assertTrue(userServices.newUserRegistration(register));
	}
	@Ignore
	@Test
	public void  newRegistrationNeagative() {
	    Registration register = new Registration();
	    register.setCus_id("121");
	    register.setPwd("rithika120");
	    register.setAadhar_num("798920030212");
		Assert.assertFalse(userServices.newUserRegistration(register));
	}
	
	/**
	 * test case for login for both user and admin
	 */
	@Ignore
	@Test
	public void login() {
		Assert.assertTrue(userServices.login("121", "rithika120"));
	}
	@Ignore
	@Test
	public void loginNegative() {
		Assert.assertTrue(userServices.login("-1", "rithika120"));
	}
	
	
	/**
	 * test case for finding whether the user existed or not
	 */
	@Ignore
	@Test
	public void findById() {
		Assert.assertTrue(userServices.findById("121"));
	}
	@Ignore
	@Test
	public void findByIdNegative() {
		
		Assert.assertTrue(userServices.findById("-12a"));
	}
	
	
	/**
	 * test case for display all the services provided by the admin
	 */
	@Ignore
	@Test
	public void fetchAllServices() {
		Map<Integer, String> service_map = userServices.fetchAllServices();
		Assert.assertNotNull(service_map);
	}
	@Ignore
	@Test
	public void fetchAllServicesNegative() {
		Map<Integer, String> service_map = userServices.fetchAllServices();
		Assert.assertNull(service_map);
	}
	
	
	
	/**
	 * test case to raise the complaint by the user
	 */
	@Ignore
	@Test
	public void setComplaint() {
		Assert.assertTrue(userServices.setComplaint(1,"Illegal",121, "102"));
	}
	
	
	
	/**
	 * test case for the user to check the complaint status given by the user
	 */
    @Ignore
	@Test
	public void myUpdatedComplaintStatus() {
		ArrayList<UpdatedComplaints> complaintStatus = userServices.myUpdatedComplaintStatus("10");
		Iterator<UpdatedComplaints> it = complaintStatus.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
		Assert.assertNotNull(complaintStatus);
	}
	
	@Test
	public void myUpdatedComplaintStatusNegative() {
		ArrayList<UpdatedComplaints> complaintStatus1 = userServices.myUpdatedComplaintStatus("103");
		Iterator<UpdatedComplaints> it = complaintStatus1.iterator();
		System.out.println(complaintStatus1.size());
		while(it.hasNext()){
			UpdatedComplaints demo = it.next();
			System.out.println(demo.getConsumer_id()+demo.getStatus());
			
		}
		Assert.assertTrue(complaintStatus1.isEmpty());
	}
	
	
	
	/**
	 * test case for user to 
	 *  update the old password by new password
	 */
	@Ignore
	@Test
	public void updatePassword() {
		Assert.assertTrue(userServices.updatingMyPassword("121", "rithikaG"));
	}
	
	public void updatePasswordNegative() {
		Assert.assertTrue(userServices.updatingMyPassword("-121", "rithikaG"));
	}
	
	
	
}
