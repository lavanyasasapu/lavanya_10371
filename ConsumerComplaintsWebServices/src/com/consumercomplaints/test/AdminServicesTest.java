package com.consumercomplaints.test;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.services.AdminServices;
import com.consumercomplaints.services.AdminServicesIMPL;

/**
 * this class is to test all the admin operations by using JUnit testing
 * @author IMVIZAG
 *
 */
public class AdminServicesTest {
	
	AdminServices adminservices = null;
	
	/**
	 * setUp assert method to execute before each method is invoked
	 */
	@Before
	public void setUp() {
		adminservices = new AdminServicesIMPL();
	}
	
	/**
	 *  assert method to execute after each method is invoked
	 */
	@After
	public void tearDown() {
		adminservices = null;
	}
    
	/**
	 * test case for add service
	 */
	@Ignore
	@Test
	public void addService() {
		 String service ="insert";
		 Assert.assertTrue(adminservices.addService(service));
	}
	
	/**
	 * test case for delete service
	 */
	@Ignore
	@Test
	public void deleteService() {
		
		Assert.assertTrue(adminservices.deleteService(14));
	}
	@Test
	public void deleteServiceNegative() {
		
		Assert.assertTrue(adminservices.deleteService(-14));
	}
	
	
	/**
	 * test case for update service
	 */
	@Test
	public void updateService() {
		
		Assert.assertTrue(adminservices.updateService(11,"connection problem"));
	}
	@Test
	public void updateServiceNegative() {
		
		Assert.assertTrue(adminservices.updateService(-11,"connection problem"));
	}
	
	/**
	 * test case for total complaints recevied
	 */
	@Test
	public void totalComplaints() {
		
		ArrayList<Complaints> complaint_list = adminservices.totalComplaints();
		
		Assert.assertNotNull(complaint_list);
	}
	/**
	 * test case for total customers who are registered 
	 */
	@Test
	public void totalCustomers() {
		
		ArrayList<Registration> customers_list = adminservices.totalCustomers();
		
		Assert.assertNotNull(customers_list);
		}
	
	/**
	 * test case for check the user by using referenceId
	 */
	@Test
	public void checkByReferenceId() {
		Complaints complaints = adminservices.checkByReferenceId(1003);
		
		Assert.assertNotNull(complaints);
	}
	@Test
	public void checkByReferenceIdNegative() {
		Complaints complaints = adminservices.checkByReferenceId(10030);
		
		Assert.assertNotNull(complaints);
	}
	
	
	/**
	 * test case for check the user by using ServiceId
	 */

	@Test
	public void checkByServiceId() {
		ArrayList<Complaints> complaint_list=  adminservices.checkByServiceId("1");
		Assert.assertNotNull(complaint_list);
	}
	@Test
	public void checkByServiceIdNegative() {
		ArrayList<Complaints> complaint_list=  adminservices.checkByServiceId("-1");
		Assert.assertNotNull(complaint_list);
	}
	
	/**
	 * test case for check the user by using consumerId
	 */

	@Test
	public void checkByConsumerId() {
		ArrayList<Complaints> complaint_list=  adminservices.checkByConsumerId("102");
		Assert.assertNotNull(complaint_list);
	}
	@Test
	public void checkByConsumerIdNegative() {
		ArrayList<Complaints> complaint_list=  adminservices.checkByConsumerId("1020");
		Assert.assertNotNull(complaint_list);
	}
	
	
	/**
	 * test case for set the complaint status
	 * @throws ParseException
	 */
	@Test
	public void setComplaintStatus() throws ParseException {
		
		String pattern = "MM-dd-yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = simpleDateFormat.parse("23-01-2019");
		Assert.assertTrue(adminservices.setComplaintStatus(1000, "103", "it will be resolve by tomorrow", date, "No"));
	}
	
}




















