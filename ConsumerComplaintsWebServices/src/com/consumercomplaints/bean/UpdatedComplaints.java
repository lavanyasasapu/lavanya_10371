package com.consumercomplaints.bean;

import java.util.Date;
/**
 * pojo class for updated complaints
 * @author IMVIZAG
 *
 */
public class UpdatedComplaints {
	int reference_id;
	String status;
	Date date_of_resolving;
	String comments;
	int consumer_id;
	
	public int getReference_id() {
		return reference_id;
	}
	public void setReference_id(int reference_id) {
		this.reference_id = reference_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDate_of_resolving() {
		return date_of_resolving;
	}
	public void setDate_of_resolving(Date date_of_resolving) {
		this.date_of_resolving = date_of_resolving;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getConsumer_id() {
		return consumer_id;
	}
	public void setConsumer_id(int consumer_id) {
		this.consumer_id = consumer_id;
	}
	
}
