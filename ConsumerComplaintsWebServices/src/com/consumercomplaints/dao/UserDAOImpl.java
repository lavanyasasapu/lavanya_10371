package com.consumercomplaints.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.util.DBConnection;

/**
 * implementation class for user DAO
 * 
 * @author IMVIZAG
 *
 */

public class UserDAOImpl implements UserDAO {

	String person = null;
	Connection con = null;
	Statement stmt = null;
	PreparedStatement ps = null;

	/**
	 * This method is used to get All the User Services which are displayed to User.
	 */

	public Map<Integer, String> getAllServices() {

		ResultSet rs = null;
		Map<Integer, String> user_map = new HashMap<Integer, String>();

		String sql = " select * from User_Services ";
		try {
			con = DBConnection.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {

				user_map.put(rs.getInt(1), rs.getString(2));
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return user_map;
	}

	/**
	 * method for set the Complaint from user to the Complaints table.
	 */

	public boolean setCompalaints(Complaints complaints) {
          boolean flag = false;
		try {

			String sql = " insert into Complaints(complaint_id,consumer_id,service_id,complaint_name,date_of_complaint) values (?, ?, ?, ?, ? ) ";
			con = DBConnection.getCon();
			PreparedStatement ps = con.prepareStatement(sql);
//		    ps.setInt(1,null);
			ps.setInt(1, complaints.getComplaint_id());
			ps.setString(2, complaints.getConsumer_Id());
			ps.setInt(3, complaints.getService_Id());
			ps.setString(4, complaints.getComplaint_Name());
			ps.setDate(5, new Date(complaints.getDate_of_Complaint().getTime()));

			@SuppressWarnings("unused")
			int result = ps.executeUpdate();
			if(result == 1) {
				flag = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * method for new user registration.
	 */

	public boolean createUserRegistration(Registration register) {

		String role = "user";
		boolean result = false;

		try {
			con = DBConnection.getCon();
			String sql = "insert into Registration values(?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setString(1, register.getCus_id());
			ps.setString(2, register.getPwd());
			ps.setString(3, register.getAadhar_num());
			ps.setString(4, role);
			int row_count = ps.executeUpdate();
			if (row_count == 1) {
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * method for validate the user id while New registration.
	 */

	public boolean searchById(String id) {

		ResultSet rs = null;
		boolean flag = false;

		try {
			con = DBConnection.getCon();
			String sql = "select * from Registration";

			stmt = con.createStatement();

			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getString(1).equals(id) && id.length() == 3) {
					flag = true;
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * method for user login by using Id and Password it validates the user is
	 * Existed or not.
	 */

	public boolean login(String login_id, String login_password) {

		ResultSet rs = null;
		boolean flag = false;

		try {
			String sql = "select * from Registration";
			con = DBConnection.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if ((rs.getString(1).equals(login_id)) && (rs.getString(2).equals(login_password))) {
					flag = true;
					person = rs.getString(4);
					break;
				} else {
					flag = false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * method for validate the Aadhar while New Regisration.
	 */

	public boolean validateAadhar(String aadhar) {

		ResultSet rs = null;
		boolean flag = false;

		try {
			con = DBConnection.getCon();
			String sql = "select * from Registration";
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getString(3).equals(aadhar) || aadhar.length() != 12) {
					flag = true;
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * method for checking,whether the person is logging in as admin or user
	 */

	public String checkAdminOrUser() {

		String result = person;
		return result;
	}

	/**
	 * method for Reteriving the Updated complaint from Updated complaints table
	 */
	public ArrayList<UpdatedComplaints> myUpdatedComplaint(String id) {
        
		String res = id;
		System.out.println(res);
		ResultSet rs = null;
		UpdatedComplaints updatedComplaints = null;
		ArrayList<UpdatedComplaints> complaint_list = new ArrayList<UpdatedComplaints>();

		try {

			String sql = " select * from complaint_status where consumer_id='res'";
			con = DBConnection.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
                System.out.println("data updating");
				updatedComplaints = new UpdatedComplaints();
				updatedComplaints.setReference_id(rs.getInt(1));
				updatedComplaints.setConsumer_id(rs.getInt(2));
				updatedComplaints.setStatus(rs.getString(3));
				updatedComplaints.setDate_of_resolving(rs.getDate(4));
				updatedComplaints.setComments(rs.getString(5));

				complaint_list.add(updatedComplaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}

	/**
	 * this Method is used to upadted the password of Existing user after logged in.
	 */

	public boolean updateMyPassword(String consumer_id, String password) {

		boolean flag= false;
		try {

			String sql = " update Registration set password = ? where id = ? ";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setString(2, consumer_id);

			@SuppressWarnings("unused")
			int result = ps.executeUpdate();
			if(result == 1) {
				flag =  true;
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
}
