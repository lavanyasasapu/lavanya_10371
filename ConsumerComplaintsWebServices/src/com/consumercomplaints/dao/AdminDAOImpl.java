package com.consumercomplaints.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.consumercomplaints.bean.Complaints;
import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.util.DBConnection;

/**
 * Dao class for admin
 * 
 * @author IMVIZAG
 *
 */
public class AdminDAOImpl implements AdminDAO {
	/**
	 * method for add extra service which can be updated to the User Services Directly.
	 */
	public boolean addExtraService(String service) {
		boolean isAdded = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {

			String sql = " insert into User_services(Service_Name) values (?) ";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, service);
			
			@SuppressWarnings("unused")
			int result = ps.executeUpdate();
			if(result == 1) {
				isAdded = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			//closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isAdded;
	}

	/**
	 * method for removing the service.
	 * After Removing the Service it will automatically updated to the User Services.
	 */
	public boolean removeService(int key) {
		boolean isDelete = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {

			String sql = " delete from User_services where service_id = ? ";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, key);
			
			@SuppressWarnings("unused")
			int result = ps.executeUpdate();
			if(result == 1) {
				isDelete = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isDelete;
	}

	/**
	 * method for updating the service based on the key.
	 * Here Existing service can be updated and it will updated to User Services.
	 */
	public boolean modifySerive(int key, String update_service) {
		boolean isUpdate = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {

			String sql = " update User_services set service_name = ? where service_id = ? ";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setString(1, update_service);
			ps.setInt(2, key);
			
			@SuppressWarnings("unused")
			int result = ps.executeUpdate();
			if(result == 1) {
				isUpdate = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isUpdate;
	}

	/**
	 * method for retriving all the complaints details.
	 */
	public ArrayList<Complaints> allComplaints() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Complaints complaints = null;
		ArrayList<Complaints> complaint_list = new ArrayList<Complaints>();

		try {
			String sql = " select * from complaints ";
			con = DBConnection.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {

				complaints = new Complaints();
				complaints.setReference_Id(rs.getInt(1));
				complaints.setComplaint_id(rs.getInt(2));
				complaints.setConsumer_Id(rs.getString(3));
				complaints.setService_Id(rs.getInt(4));
				complaints.setComplaint_Name(rs.getString(5));
				complaints.setDate_of_Complaint(rs.getDate(6));
				complaint_list.add(complaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			 //closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}

	/**
	 * method for retriving all the Registered customers Details.
	 */
	public ArrayList<Registration> allCustomers() {

		Registration registration = null;

		Connection con = null;
		Statement stmt = null;
		ResultSet result_set = null;
		ArrayList<Registration> registration_list = new ArrayList<Registration>();

		try {
			String sql = " select * from Registration where role = 'user' ";
			con = DBConnection.getCon();
			stmt = con.createStatement();
			result_set = stmt.executeQuery(sql);
			while (result_set.next()) {
				registration = new Registration();
				registration.setCus_id(result_set.getString(1));
				registration.setPwd(result_set.getString(2));
				registration.setAadhar_num(result_set.getString(3));
				registration_list.add(registration);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			//closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return registration_list;
	}

	/**
	 * method for searching the complaint status based on the reference ID.
	 */
	public Complaints modifyCompliantStatus(int search) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet result_set = null;
		Complaints complaints = new Complaints();
		try {

			String sql = " select * from Complaints where reference_Id = ?";
			con = DBConnection.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, search);
			result_set = ps.executeQuery();

			while (result_set.next()) {

				complaints.setReference_Id(result_set.getInt(1));
				complaints.setComplaint_id(result_set.getInt(2));
				complaints.setConsumer_Id(result_set.getString(3));
				complaints.setService_Id(result_set.getInt(4));
				complaints.setComplaint_Name(result_set.getString(5));
				complaints.setDate_of_Complaint(result_set.getDate(6));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// closing the connection
			try {
				con.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaints;
	}

	/**
	 * method for inserting the updated complaint status from Admin to the Updated Complaints table.
	 */
	public boolean insertUpdatedComplaint(UpdatedComplaints updatedComplaints) {
		Connection con = null;
		PreparedStatement ps = null;
		String sql;
		boolean result = false;

		try {
			con = DBConnection.getCon();
			sql = "insert into complaint_status values(?,?,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, updatedComplaints.getReference_id());
			ps.setInt(2, updatedComplaints.getConsumer_id());
			ps.setString(3, updatedComplaints.getStatus());
			ps.setDate(4, new Date(updatedComplaints.getDate_of_resolving().getTime()));
			ps.setString(5, updatedComplaints.getComments());

			int row_count = ps.executeUpdate();
			if (row_count == 1) {
				result = true;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
    /**
     * method for retriving the all complaints by Using the Service ID from Complaints Table.
     */
	public ArrayList<Complaints> allComplaintsByServiceId(String service_id) {

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Complaints complaints = null;
		ArrayList<Complaints> complaint_list = new ArrayList<Complaints>();
		
		try {
			String sql = "SELECT * FROM Complaints WHERE service_id = ?";
			con = DBConnection.getCon();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, service_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {

				complaints = new Complaints();
				complaints.setReference_Id(rs.getInt(1));
				complaints.setComplaint_id(rs.getInt(2));
				complaints.setConsumer_Id(rs.getString(3));
				complaints.setService_Id(rs.getInt(4));
				complaints.setComplaint_Name(rs.getString(5));
				complaints.setDate_of_Complaint(rs.getDate(6));

				complaint_list.add(complaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			 //closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}
	
	
	
	 /**
     * method for retriving the all complaints by Using the consumer ID from Complaints Table.
     */
	public ArrayList<Complaints> allComplaintsByConusumerId(String consumer_id) {

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Complaints complaints = null;
		ArrayList<Complaints> complaint_list = new ArrayList<Complaints>();
		
		try {
			String sql = "SELECT * FROM Complaints WHERE consumer_id = ?";
			con = DBConnection.getCon();
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1,consumer_id );
			rs = pstmt.executeQuery();
			while (rs.next()) {

				complaints = new Complaints();
				complaints.setReference_Id(rs.getInt(1));
				complaints.setComplaint_id(rs.getInt(2));
				complaints.setConsumer_Id(rs.getString(3));
				complaints.setService_Id(rs.getInt(4));
				complaints.setComplaint_Name(rs.getString(5));
				complaints.setDate_of_Complaint(rs.getDate(6));

				complaint_list.add(complaints);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			 //closing the connection
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return complaint_list;
	}

	
	
}
