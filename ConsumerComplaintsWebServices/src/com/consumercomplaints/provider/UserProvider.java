package com.consumercomplaints.provider;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.consumercomplaints.bean.Registration;
import com.consumercomplaints.bean.UpdatedComplaints;
import com.consumercomplaints.services.UserServicesIMPL;

/**
 * This is the resource class which acts as web resource which is specified with
 * a URL
 * 
 * @author IMVIZAG
 *
 */
@Path("/UserProvider")
public class UserProvider {
	/**
	 * This is resource method for user registration
	 * @param cus_id
	 * @param password
	 * @param aadhar
	 * @return
	 */
	@POST
	@Path("/newRegistration")
	public String newRegistration(@QueryParam("cus_id") String cus_id, @QueryParam("password") String password, @QueryParam("aadhar") String aadhar) {
		Registration register = new Registration();
		register.setCus_id(cus_id);
		register.setPwd(password);
		register.setAadhar_num(aadhar);
		boolean isRegistered = new UserServicesIMPL().newUserRegistration(register);	
		return isRegistered ? "success" : "something wrong";
	}
	
	/**
	 * This is resource method for login
	 * @param cus_id
	 * @param password
	 * @return
	 */
	@POST
	@Path("/login")
	public String login(@QueryParam("cus_id") String cus_id, @QueryParam("password") String password) {
		boolean isLogin = new UserServicesIMPL().login(cus_id, password);
		return isLogin ? "login successful" : "login failed";
	}
	
	/**
	 * This is the service method to fetch all the services provided by the admin
	 */
	@GET
	@Path("/fetchAllServices")
	@Produces("application/json")
	public Map<Integer, String> fetchAllServices() {
		Map<Integer, String> allServices = new UserServicesIMPL().fetchAllServices();
		return allServices;
	}
	
	/**
	 * This is the service method to raise the complaint
	 * 
	 */
	@POST
	@Path("/setComplaint")
	public String setComplaint(@QueryParam("service_Id") int service_Id, @QueryParam("complaint_Name") String complaint_Name,  @QueryParam("complaint_id") int complaint_id,@QueryParam("user_id") String user_id ) {
		boolean complaintMade = new UserServicesIMPL().setComplaint(service_Id, complaint_Name, complaint_id, user_id);
		return complaintMade ? "complaint raised" : "something wrong";
	}
	/**
	 * This is a service method to see the complaint status given by the admin
	 * @param consumerID
	 * @return
	 */
	@GET
	@Produces("application/json")
	@Path("/myUpdatedComplaintStatus")
	public ArrayList<UpdatedComplaints> myUpdatedComplaintStatus(@QueryParam("consumerID") String consumerID){
		ArrayList<UpdatedComplaints>  myStatus= new UserServicesIMPL().myUpdatedComplaintStatus(consumerID);
		return myStatus;
		
	}
	/**
	 * This is a service method to update the user password
	 * @param consumer_id
	 * @param password
	 * @return
	 */
	@POST
	@Path("/updatePassword")
	public String updatePassword(@QueryParam("consumer_id") String consumer_id, @QueryParam("password") String password) {
		boolean passwordUpdated=  new UserServicesIMPL().updatingMyPassword(consumer_id, password);
		return passwordUpdated ? "Password Updated" : "Updation fails";
	}
	
	
	
	

}
