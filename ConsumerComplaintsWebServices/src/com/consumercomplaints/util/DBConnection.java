package com.consumercomplaints.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	public static Connection getCon() {
		Connection con = null;
		
		try {
			//loading the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//establish the connection
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/consumercomplaints", "root", "innominds");
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}		
		return con;
	}	
}

