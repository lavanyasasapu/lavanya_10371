<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="3">
<tr>
<th>Id</th>
<th>Name</th>
<th>Age</th>
<th>City</th>
<th>State</th>
<th>PinCode</th>
</tr>
<tr>
<td> ${st.id }</td>
<td> ${st.name }</td>
<td> ${st.age }</td>
<td> ${st.address.city }</td>
<td> ${st.address.state }</td>
<td> ${st.address.pincode }</td>
</tr>
</table>
</body>
</html>