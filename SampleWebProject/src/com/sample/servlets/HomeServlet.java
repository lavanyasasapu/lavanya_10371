package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/insert")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used to get the request from the html
	 * page and sends back the response to the user in the form of html
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		
		String name = request.getParameter("name");
		int id = Integer.parseInt(request.getParameter("id"));
		int age = Integer.parseInt(request.getParameter("age"));
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String pincode = request.getParameter("pincode");
		
		StudentService studentServie = new StudentServiceImpl();
		
		Student student = new Student();
		Address address = new Address();
		address.setCity(city);
		address.setCity(state);
		address.setPincode(pincode);
		student.setId(id);
		student.setName(name);
        student.setAge(age);
        student.setAddress(address);
		
		boolean flag = studentServie.insertStudent(student);
//		PrintWriter out = response.getWriter();
//		out.println("<html><body>");
//		if(flag == true) {
//			out.println("<h3>registered successfully ..</h3>");
//		}
//		else {
//			out.println("<h3>something wrong</h3>");
//		}
//		out.println("</body></html>");
//		out.close();
		request.setAttribute("flag", flag);
		RequestDispatcher rd = request.getRequestDispatcher("insert.jsp");
		rd.forward(request, response);
		
	}

}
