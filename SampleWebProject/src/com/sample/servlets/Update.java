package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//reading the id from the Update.html and storing it into the variable 'id'
		int sid = Integer.parseInt(request.getParameter("id"));
		
		//reading the id from the Update.html and storing it into the variable 'sname'
		String sname = request.getParameter("sname");
		
		//instance creation for StudentServiceImpl class
		StudentService studentService = new StudentServiceImpl();
		
		//calling the updateStudent
		boolean flag = studentService.updateStudent(sid, sname);
		
		//creating printWriter
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.print("<h3>"+sid+"</h3>");
		out.print("<h3>"+sname+"</h3>");
		if(flag) {
			out.println("<h3>value updated</h3>");
			
		}
		else {
			out.println("<h3> something wrong in updation</h3>");
		}
		out.println("</body></html>");

		//close the print writer
		out.close();
	}

}
