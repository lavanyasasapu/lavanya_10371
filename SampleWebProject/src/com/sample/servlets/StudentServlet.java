package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//reading the id, name, age from the index.html and storing it into the variable 'id', 'name', 'age'
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
		
		//creating the student class object ans setting the methods
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		
        //create an object and call the insertStudent method
		StudentService service = new StudentServiceImpl();
		boolean result = service.insertStudent(student);
				
		//Creating the object of printWrietr class
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		
		if(result) {
			out.print("<h2>Student Saved</h2>");
		}
		else {
			out.print("<h2>Something wrong</h2>");
		}
		
		out.print("</body></html>");
		//closing the PrintWriter
		out.close();
		
		
	}

}
