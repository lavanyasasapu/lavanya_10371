package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Search() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//reading the id from theSearch. html and storing it into the variable 'id'
		int id = Integer.parseInt(request.getParameter("id"));
		
		//instance creation for StudentServiceImpl class
		StudentService studentService = new StudentServiceImpl();
		
		//calls findById method
		Student student = studentService.findById(id);
		
		//creating printWriter
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		out.println("<h3>" + student.getId() + " " + student.getName() + " " + student.getAge() + "</h3>");
		out.print("</body></html>");
		
		//closing the PrintWriter
		out.close();

	}

}
