package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class delete
 */
@WebServlet("/delete")
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		//reading the id from the html and storing it into the variable 'id'
	int  id = Integer.parseInt(request.getParameter("id"));
	
	//instance creation for StudentServiceImpl class
	StudentService studentService = new StudentServiceImpl();
	//calls deleteStudent Method
	boolean flag = studentService.deleteStudent(id);
	
	//creating printWriter
	PrintWriter out = response.getWriter();
	out.print("<html><body>");
	
	if(flag) {
		out.print("<h2>Student Deleted</h2>");
	}
	else {
		out.print("<h2>Something wrong</h2>");
	}
	
	out.print("</body></html>");
	//closing the PrintWriter
	out.close();
	}

}
