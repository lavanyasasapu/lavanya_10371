package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

public interface StudentService {
	
	boolean insertStudent( Student student );
	
	Student findById(int id);
	
	List<Student> fetchAllStudents();
	boolean deleteStudent(int id);
	boolean updateStudent(int id, String value_to_update);
	
	
}
