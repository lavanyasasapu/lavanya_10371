package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;


public class StudentServiceImpl implements StudentService{

	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		
		return result;
	}

	@Override
	public Student findById(int id) {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	@Override
	public boolean deleteStudent(int id) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean flag = studentDAO.deleteStudent(id);
		return flag;
	}

	@Override
	public boolean updateStudent(int id, String value_to_update) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean flag = studentDAO.updateStudent(id, value_to_update);
		return flag;
	}

}














