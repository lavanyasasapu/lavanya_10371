package com.sample.dao;

import java.util.List;

import com.sample.bean.Student;

public interface StudentDAO {
	
	boolean createStudent( Student student );
	
	Student searchById(int id);
	
	List<Student> getAllStudents();
	
	boolean deleteStudent(int id);
	
	boolean updateStudent(int id, String  value_to_update);
	
}
