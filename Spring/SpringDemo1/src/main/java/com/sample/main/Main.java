package com.sample.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.sample.bean.Cart;

public class Main {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		ApplicationContext app = new ClassPathXmlApplicationContext("classpath:com/sample/main/Sample.xml");
		
		Cart cart = (Cart) app.getBean("cart");
		System.out.println(cart);
//		System.out.println(cart.getCartName());
//		System.out.println(cart.getItems().getItemId());
//		System.out.println(cart.getItems().getItemName());
//		
	}

}
