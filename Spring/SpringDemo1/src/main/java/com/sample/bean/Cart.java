package com.sample.bean;

public class Cart {
 private String cartName; 
 @Override
public String toString() {
	return "Cart [cartName=" + cartName + ", items=" + items  + "]";
}
private Items items;
public String getCartName() {
	return cartName;
}
public void setCartName(String cartName) {
	this.cartName = cartName;
}
public Items getItems() {
	return items;
}
public void setItems(Items items) {
	this.items = items;
}
	
}
