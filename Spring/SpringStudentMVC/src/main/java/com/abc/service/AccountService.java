package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {
	
	Account getAccountById(int accNo);
	
	void createAccount(Account account);
	
	void deleteAccount(int accNo);

}
