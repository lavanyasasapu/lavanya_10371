package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDao;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServiceIMPL implements AccountService {
	
	@Autowired
	private AccountDao accountDao;
	
	@Transactional(readOnly = true)
	@Override
	public Account getAccountById(int accNo) {
		
		return accountDao.getAccountById(accNo);
	}
    @Transactional
	@Override
	public void createAccount(Account account) {
		
		 accountDao.createAccount(account);
	}
    @Transactional
	@Override
	public void deleteAccount(int accNo) {
		accountDao.deleteAccount(accNo);
		
	}

}
