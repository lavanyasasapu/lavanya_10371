package com.abc.hibernate.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Account")
public class Account {
	@Id
	@Column(name = "accNo")
	private int accNo;
	@Column(name = "accName")
	private String accName;
	@Column(name = "accBal")
	private double balance;
	
	public int getAccNo() {
		return accNo;
	}
	public void setAccno(int accNo) {
		this.accNo = accNo;
	}
	public String getAccName() {
		return accName;
	}
	public void setName(String name) {
		this.accName = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	

}
