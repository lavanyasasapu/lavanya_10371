package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	/**
	 * method for fetch based on the account Number
	 * @param accNo
	 * @param map
	 * @return
	 */
	@RequestMapping("/accounts/{id}")
	public String searchById(@PathVariable("id") int accNo, ModelMap map) {
		
		Account account = accountService.getAccountById(accNo);
		
		map.addAttribute("account", account);
		return "account_saved";
		
	}
	
	@RequestMapping("/createForm")
	public String createForm() {
		
		return "createAccount";
		
	}
	@PostMapping("/createAccount")
	public String createAccount(@ModelAttribute Account account, ModelMap map) {
		accountService.createAccount(account);
		return "AccountCreated";
			
	}
	@RequestMapping("/deleteAccount/{id}")
public String deleteAccount(@PathVariable("id") int accNo, ModelMap map) {
	accountService.deleteAccount(accNo);
		return "AccountDeleted";
	
	
}

}
