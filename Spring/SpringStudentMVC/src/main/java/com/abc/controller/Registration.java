package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.abc.bean.Register;

@Controller
public class Registration {
	

	@GetMapping("registerform")
	public String getRegisterForm() {
		return "Registration";
	}
	@PostMapping("registration")
	public String doRegister(@ModelAttribute Register register,ModelMap map) {
		
		if(register.getPassword().length() < 3) {
		   return "FailureRegistration";	
		}
		else {
//			map.addAttribute("userDetails", user);
			return "SuccessRegistration";
		}
		
		
	}

}
