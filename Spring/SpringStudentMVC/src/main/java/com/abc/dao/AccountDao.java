package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDao {
	
	Account getAccountById(int accNo);
	
	void  createAccount(Account account);
	
	void deleteAccount(int accNo);

}
