package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * method for search
	 */
	@Override
	public Account getAccountById(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}
	
	public void createAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
	
	}
	public void deleteAccount(int accNo) {
		Account account = (Account) sessionFactory.getCurrentSession().load(
	               Account.class,accNo);
	        if (null != account) {
	            this.sessionFactory.getCurrentSession().delete(account);
	        }
	        else {
	        	System.out.println("hai");
	        }
	}

}
