package com.abc.dao;

import org.springframework.data.repository.CrudRepository;

import com.abc.entity.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {

}
