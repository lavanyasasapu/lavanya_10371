package com.abc.service;

import java.util.List;

import com.abc.entity.Account;

public interface AccountService {
	
public Account save(Account account);
	
	public List<Account> findAll();
	
	public Account findAccountById(int accno);
	
	public void deleteAccount(int accno);
	public Account updateAccount(Account account) ;

}
