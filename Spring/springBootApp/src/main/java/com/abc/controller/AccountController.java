package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.entity.Account;
import com.abc.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController {
	
	@Autowired
	private AccountService accountService;

	@PostMapping("/account/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		Account newAccount = accountService.save(account);
		     
		 return new ResponseEntity<>("New account is created with id:" + newAccount.getAccNo(),HttpStatus.CREATED);
		}
	
	  @GetMapping("/account")
	   public ResponseEntity<List<Account>> list() {
	      List<Account> accounts = accountService.findAll();
	      if(accounts.isEmpty()){
	            return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
	        }
	        return new ResponseEntity<List<Account>>(accounts, HttpStatus.OK);
	   }

	
}
