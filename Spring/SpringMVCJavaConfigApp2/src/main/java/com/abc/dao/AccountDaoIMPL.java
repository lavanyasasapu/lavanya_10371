package com.abc.dao;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.entities.Account;

@Repository
public class AccountDaoIMPL implements AccountDao{
	
	

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public int  createAccount(Account account) {
		
		Session session = sessionFactory.getCurrentSession();
		session.save(account);
		return account.getAccNo();
	}

	@Override
	public boolean deleteAccount() {
		
		return false;
	}

	@Override
	public boolean updateAccount() {
		
		return false;
	}

//	@Override
//	public List<Account> getAllAccounts(int accNo) {
//		 Session session = sessionFactory.getCurrentSession();
//	      CriteriaBuilder cb = session.getCriteriaBuilder();
//	      CriteriaQuery<Account> cq = cb.createQuery(Account.class);
//	      Root<Account> root = cq.from(Account.class);
//	      cq.select(root);
//	      Query query = session.createQuery(cq);
//	      return query.getResultList();
//	}

	@Override
	public Account searchAccount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account getAllAccounts(int accNo) {
		// TODO Auto-generated method stub
		return null;
	}

}
