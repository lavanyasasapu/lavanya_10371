package com.abc.dao;

import com.abc.entities.Account;

public interface AccountDao {

public int createAccount(Account account);
	
	public boolean deleteAccount();
	
	public boolean updateAccount();
	
	public Account getAllAccounts(int accNo);
	
	public Account searchAccount();
		
}
