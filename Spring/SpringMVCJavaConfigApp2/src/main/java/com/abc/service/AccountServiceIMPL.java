package com.abc.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.dao.AccountDao;
import com.abc.entities.Account;
@Service
public class AccountServiceIMPL implements AccountServcie {

	@Autowired
	private AccountDao accountDao;
	
	@Override
	@Transactional
	public int createAccount(Account account) {
		
		return accountDao.createAccount(account);
	}

	@Override
	public boolean deleteAccount() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateAccount() {
		// TODO Auto-generated method stub
		return false;
	}

//	@Override
//	public List<Account> getAllAccounts(int accNo) {
//		accountDao.createAccount(accNo);
//		return null;
//	}

	@Override
	public Account searchAccount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Account getAllAccounts(int accNo) {
		// TODO Auto-generated method stub
		return null;
	}

}
