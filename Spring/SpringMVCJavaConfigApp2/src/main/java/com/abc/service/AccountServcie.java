package com.abc.service;

import com.abc.entities.Account;

import antlr.collections.List;

public interface AccountServcie{
	
	public int createAccount(Account account);
	
	public boolean deleteAccount();
	
	public boolean updateAccount();
	
	public Account getAllAccounts(int accNo);
	
	public Account searchAccount();
		
	

}
