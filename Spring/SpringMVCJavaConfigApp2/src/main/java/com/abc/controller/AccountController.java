package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.abc.entities.Account;
import com.abc.service.AccountServcie;

@RestController
public class AccountController {
	

	@Autowired
	private AccountServcie accountService;
	
	@PostMapping("/account/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		int id = accountService.createAccount(account);
		 
    return new  ResponseEntity<String>("New account is created with id:" + id,HttpStatus.CREATED);
		}
	
//	@GetMapping("/account/{id}")
//	   public ResponseEntity<Account> get(@PathVariable("accNo") int accNo) {
//	      Account account = (Account) accountService.getAllAccounts(accNo);
//	      if (account == null) {
//	            
//	            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
//	        }
//	        return new ResponseEntity<Account>(account, HttpStatus.OK);
//	   }

}
