package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import com.abc.bean.Employee;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/config/config.xml");
		Employee employee = (Employee) context.getBean("emp");
		System.out.println("Employee details");
		System.out.println(employee.getEmpId());
		System.out.println(employee.getAddress().getState());

	}

}
