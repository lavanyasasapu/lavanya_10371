package com.demo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.demo.bean.Employee;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext apc = new ClassPathXmlApplicationContext("classpath:com/demo/main/context.xml");
		Employee emp = (Employee) apc.getBean("emp");
		System.out.println("Employee name    : " +emp.getEmpName());
		System.out.println("Employee Branch  :" +emp.getBranch().getBranchName());
		System.out.println("Employee Address :" +emp.getBranch().getBranchAddr());
		
	}

}
